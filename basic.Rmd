---
title: "MYEDA DASHBOARD"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: scroll
    social: [ "twitter", "facebook", "linkedin" ]
    css: styles.css
runtime: shiny
---

```{r setup, include=FALSE}
library(flexdashboard)
library(highcharter)
library(data.table)
library(networkD3)
library(tidyverse)
library(rmarkdown)
library(tidytext)
library(readxl)
library(skimr)
library(shiny)
library(xlsx)
library(DT)
```

File Upload 
=======================================================================

Sidebar Upload {.sidebar data-width=250}
-----------------------------------------------------------------------

Bagian ini merupakan bagian utama, di mana anda harus memilih jenis file yang di akan upload dan di analisis. Secara umum, file berupa table baik dalam format csv atau excel yang terdiri dari `1 kolom = 1 variabel`, dan `1 baris = 1 observasi`. Untuk lebih jelasnya dapat melihat contoh data berikut.


```{r}
# Pilih file -----------------------------------------------------------------------
radioButtons(inputId = "tipefile", label = "Choose file type", 
             choices = c("csv", "excel"), selected = "excel", inline = TRUE)

# upload file ----------------------------------------------------------------------
conditionalPanel(condition = "input.tipefile == 'csv'", 
                 fileInput(inputId = "filecsv", 
                           label = "Choose your csv file", 
                           multiple = TRUE, 
                           accept = c("text/csv",
                                      "text/comma-separated-values, text/plain",
                                      ".csv"), 
                           buttonLabel = "Upload", 
                           placeholder = "drop csv file here"))

conditionalPanel(condition = "input.tipefile == 'excel'", 
                 fileInput(inputId = "filexls", 
                           label = "Choose your excel file", 
                           multiple = TRUE, 
                           accept = c(".xls", ".xlsx"), 
                           buttonLabel = "Upload", 
                           placeholder = "drop excel file here"))

# file header -----------------------------------------------------------------------
checkboxInput(inputId = "header", label = "My file have a header",
              value = TRUE)


# file separator --------------------------------------------------------------------
conditionalPanel(condition = "input.tipefile == 'csv'", 
                 radioButtons("sep", "Separator", inline = TRUE,
                   choices = c(Comma = ",",
                               Semicolon = ";",
                               Tab = "\t"),
                   selected = ";"))

# quotes ----------------------------------------------------------------------------
conditionalPanel(condition = "input.tipefile == 'csv'",
                 radioButtons("quote", "Quote", inline = TRUE,
                   choices = c(None = "",
                               "Double" = '"',
                               "Single" = "'"),
                   selected = '"'))
```

Vis Table {data-width=750}
-----------------------------------------------------------------------
###

```{r}
# data
df <- reactive({
  if (input$tipefile == "csv") {
    req(input$filecsv)
    df <- read_delim(input$filecsv$datapath, col_names = input$header, 
                 delim = input$sep, quote = input$quote)
  }
  else {
    req(input$filexls)
    df <- read_excel(input$filexls$datapath, col_names = input$header)
  }
})

# vis
renderDataTable({
  datatable(df(), options = list(searching = TRUE, 
                               lengthMenu = list(c(2,4,5,6,7,8, -1), 
                                                 c('2','4','5','6','7','8', "All"))))
})
```


Term Frequency
=======================================================================

Sidebar TF {.sidebar}
--------------------------------------------------------------------------------------

Term Frequency digunakan untuk mengetahui term/kata yang sering digunakan dalam sebuah dokumen. Tabel digunakan untuk melihat seluruh term yang ada. Plot digunakan untuk melihat term dengan angka tertinggi. Pengaturan `Jumlah token` untuk menentukan term onegram/bigram/dst. `Top term` untuk menentukan jumlah term yang ditampilakan dalam plot.

```{r FreqControl}
# token ------------------------------------------------------------------------------
selectInput("tokenise", "Token", 
            choices = c("onegram" = 1, "bigram" = 2, "trigram" = 3), 
            selected = "onegram")

# topwords ---------------------------------------------------------------------------
selectInput("mytop", "Top term frekuensi (plot)", choices = c(1:30), 
            selected = 10)

# kolom -----------------------------------------------------------------------------
numericInput("kolom", "No. kolom analisis", 3, 
             min = 1, max = 30)
```

Vis TF {data-height=580}
-----------------------------------------------------------------------
###
```{r}
# file -------------------------------------------------------------------------------
subset_dataset <- reactive({
  if (input$tipefile == "csv") {
    req(input$filecsv)
    df_count <- read_delim(input$filecsv$datapath, col_names = input$header, 
                           delim = input$sep, quote = input$quote)
    df_count <- df_count %>%
      select(mycol = input$kolom) %>%
      unnest_tokens(kata, mycol, token = "ngrams", n = as.integer(input$tokenise), 
                    to_lower = TRUE) %>%
      count(kata, sort = TRUE)
  }
  else {
    req(input$filexls)
    df_count <- read_excel(input$filexls$datapath, col_names = input$header)
    df_count <- df_count %>%
      select(mycol = input$kolom) %>%
      unnest_tokens(kata, mycol, token = "ngrams", n = as.integer(input$tokenise), 
                    to_lower = TRUE) %>%
      count(kata, sort = TRUE)
  }
})

# visual yang tampil ----------------------------------------------------------------
renderDataTable({
  datatable(subset_dataset(), options = list(searching = TRUE,
                               lengthMenu = list(c(10,7, 4, 3,-1),
                                                 c('10','7',"4","3","All"))))
})

# download action -------------------------------------------------------------------
downloadHandler(filename = function() {
  req(input$start)
  
  paste('Term Frequency ', as.character(input$tokenise), "-gram", '.csv', sep='')
   },
     content = function(file) {
     write.csv(subset_dataset(), file, row.names = FALSE)
   }
)
```

###

```{r}
renderHighchart({
  if (input$tipefile == "csv") {
    req(input$filecsv)
    
    df_count <- read_delim(input$filecsv$datapath, col_names = input$header,
                     delim = input$sep, quote = input$quote)
    df_count %>%
      select(mycol = input$kolom) %>%
      unnest_tokens(kata, mycol, token = "ngrams", n = as.integer(input$tokenise), 
                    to_lower = TRUE) %>%
      count(kata, sort = TRUE) %>%
      top_n(as.integer(input$mytop)) %>%
      arrange(desc(n)) %>%
      ungroup() %>%
      hchart(showInLegend = FALSE, "bar", hcaes(x = kata, y = n)) %>%
      hc_add_theme(hc_theme_flat()) %>%
      hc_exporting(enabled = TRUE) %>%
      hc_credits(enabled = TRUE, text = paste0("Sumber: Fahmi, U. et al., diakses pada: ", 
                                                 as.character(Sys.Date())),
                   href = "https://www.atlantis-press.com/proceedings/aapa-18/25896109")
    }
  else {
    req(input$filexls)
    
    df_count <- read_excel(input$filexls$datapath, col_names = input$header)
    df_count %>%
      select(mycol = input$kolom) %>%
      unnest_tokens(kata, mycol, token = "ngrams", n = as.integer(input$tokenise), 
                    to_lower = TRUE) %>%
      count(kata, sort = TRUE) %>%
      top_n(as.integer(input$mytop)) %>%
      arrange(desc(n)) %>%
      ungroup() %>%
      hchart(showInLegend = FALSE, "bar", hcaes(x = kata, y = n)) %>%
      hc_add_theme(hc_theme_flat()) %>%
      hc_exporting(enabled = TRUE) %>%
      hc_credits(enabled = TRUE, text = paste0("Sumber: Fahmi, U. et al., diakses pada: ", 
                                                 as.character(Sys.Date())),
                   href = "https://www.atlantis-press.com/proceedings/aapa-18/25896109")
    }
  })
```

Basic SEMNET
=======================================================================

Sidebar Semnet {.sidebar}
--------------------------------------------------------------------------------------

Semantic network digunakan untuk melihat keterhubungan antar term berdasarkan posisinya dalam dokumen. Pengaturan sebelah kiri digunakan untuk mengatur `kecerahan`. Pengaturan sebelah kanan digunakan untuk membatasi term yang ditampilkan berdasarkan `nilai minimal`. Jika term network terlalu sedikit coba turunkan nilai minimalnya. `Scroll` untuk memperbesar sementara `klik` dan `geser` kata untuk memperjelas term yang saling terhubung.

```{r semnetControl}
sliderInput("mytop1", "Nilai minimal", min = 1, max = 30, 
            step = 1, value = 10)

sliderInput("opacity", "Kecerahan", 1, 
                  min = 0.1,
                  max = 1, 
                  step = .1)

downloadButton(outputId = "downloadNet", label = "download netwok")
```

Vis Semnet {data-height=580}
--------------------------------------------------------------------------------------

###

```{r semNetOutput}
networkD3::renderSimpleNetwork({
  if (input$tipefile == "csv") {
    req(input$filecsv)
    
    df_count <- read_delim(input$filecsv$datapath, col_names = input$header,
                     delim = input$sep, quote = input$quote)
    networkData <- df_count %>%
      select(mycol = input$kolom) %>%
      unnest_tokens(kata, mycol, token = "ngrams", n = 2, to_lower = FALSE) %>%
      count(kata, sort = TRUE) %>%
      filter(n >= input$mytop1) %>%
      separate(kata, into = c("src", "target"), sep = " ")
      data.frame()
      
    simpleNetwork(networkData, opacity = input$opacity, zoom = TRUE, fontSize = 14,
                  linkColour = "#666", linkDistance = 150, nodeColour = "#ef8802")
    
    }
  else {
    req(input$filexls)
    df_count <- read_excel(input$filexls$datapath, col_names = input$header)
    
    networkData <- df_count %>%
      select(mycol = input$kolom) %>%
      unnest_tokens(kata, mycol, token = "ngrams", n = 2, to_lower = FALSE) %>%
      count(kata, sort = TRUE) %>%
      filter(n >= input$mytop1) %>%
      separate(kata, into = c("src", "target"), sep = " ")
      data.frame()
      
    simpleNetwork(networkData, opacity = input$opacity, zoom = TRUE, fontSize = 14,
                  linkColour = "#666", linkDistance = 150, nodeColour = "#ef8802")
    
    }
  })
```
